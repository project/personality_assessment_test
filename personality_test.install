<?php

/**
 * @file
 * Install, update and uninstall functions for the personality_test module.
 */

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;

/**
 * Implements hook_install().
 */
function personality_test_install() {
  // Check if the content type already exists.
  if (!NodeType::load('personality_assessment_test')) {
    // Install default node type configuration.
    \Drupal::service('config.installer')->installDefaultConfig('module', 'personality_test');
    // Create content type.
    $type = [
      'type' => 'personality_assessment_test',
      'name' => 'Personality-Assessment-Test',
      'description' => 'This is a custom content type created by personality_test Module.',
      'help' => '',
      'new_revision' => TRUE,
      'preview_mode' => 'disabled',
      'display_submitted' => FALSE,
      'preview' => [
        'preview_mode' => 'hidden',
      ],
      'field_ui_base_route' => 'entity.personality_assessment_test.edit_form',
    ];
    NodeType::create($type)->save();

    personality_test_create_field('field_personality_test_user_id', 'User', 'This is an additional custom field created by personality_test Module.', TRUE, 'entity_reference', 'user', 'personality_assessment_test', NULL, 4);
    personality_test_create_field('field_personality_test_result', 'Final Result', 'This is the body field for the personality assessment test.', TRUE, 'text_with_summary', NULL, 'personality_assessment_test', NULL, 5);
  }
}

/**
 * Helper function to create a field.
 */
function personality_test_create_field($field_name, $label, $description, $required, $type, $target_type, $bundle, $target_vocabulary, $max_length = NULL, $default_checked = FALSE) {

  if (!FieldStorageConfig::loadByName('node', $field_name)) {
    $cardinality = ($type == 'string_long' || $target_vocabulary == 'personality_terms') ? 4 : 1;

    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => $type,
      'cardinality' => $cardinality,
      'translatable' => TRUE,
      'settings' => [
        'target_type' => $target_type,
        'handler' => 'default',
        'max_length' => $max_length,
      ],
      'module' => 'personality_test',
      'status' => 1,
    ]);
    $field_storage->save();

    // Create field config.
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $bundle,
      'label' => $label,
      'description' => $description,
      'required' => $required,
      'status' => 1,
    ]);

    $field->save();
    // Load or create form display.
    $entity_form_display_repository = \Drupal::entityTypeManager()->getStorage('entity_form_display');
    $form_display = $entity_form_display_repository->load('node.' . $bundle . '.default');
    if (!$form_display) {
      $form_display = $entity_form_display_repository->create([
        'targetEntityType' => 'node',
        'bundle' => $bundle,
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }

    // Set component on form display.
    $form_display->setComponent($field_name, [
      'type' => 'string_textfield',
      'weight' => 0,
    ]);

    // Save form display.
    $form_display->save();
    // Load or create form display.
    $entity_display_repository = \Drupal::entityTypeManager()->getStorage('entity_form_display');
    $form_display = $entity_display_repository->load('node.' . $bundle . '.default');
    if (!$form_display) {
      $form_display = $entity_display_repository->create([
        'targetEntityType' => 'node',
        'bundle' => $bundle,
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }

    // Set component on form display.
    $form_display->setComponent($field_name, [
      'type' => 'string_textfield',
      'weight' => 0,
    ]);

    // Save form display.
    $form_display->save();
  }

  // Load or create view display.
  $entity_view_display_repository = \Drupal::entityTypeManager()->getStorage('entity_view_display');
  $view_display = $entity_view_display_repository->load('node.' . $bundle . '.default');
  if (!$view_display) {
    $view_display = $entity_view_display_repository->create([
      'targetEntityType' => 'node',
      'bundle' => $bundle,
      'mode' => 'default',
      'status' => TRUE,
    ]);
  }

  // Set component on view display.
  $view_display->setComponent($field_name, [
    'type' => 'string',
    'weight' => 0,
  ]);

  // Save view display.
  $view_display->save();

}
