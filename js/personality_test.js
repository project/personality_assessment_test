// Define a module for the Disc personality test
var DiscModule = (function () {
    var d, i, s, c, d2, i2, s2, c2;

    // Function to calculate scores based on user input
    function getScores() {
        d = i = s = c = 0;
        var inputs = document.getElementsByTagName("input");
        for (var count = 0, l = inputs.length; count < l; count++) {
            if (inputs[count].checked) {
                switch (inputs[count].value) {
                    case "d-most":
                        d++;
                        break;
                    case "i-most":
                        i++;
                        break;
                    case "s-most":
                        s++;
                        break;
                    case "c-most":
                        c++;
                        break;
                    case "d-least":
                        d--;
                        break;
                    case "i-least":
                        i--;
                        break;
                    case "s-least":
                        s--;
                        break;
                    case "c-least":
                        c--;
                        break;
                }
            }
        }
        return { d, i, s, c };

    }

    // Function to calculate weighted scores
    function calculateWeightedScores() {
        if (d >= 6) d2 = 7;
        else if (d >= -1) d2 = 6;
        else if (d >= -5) d2 = 5;
        else if (d >= -9) d2 = 4;
        else if (d >= -13) d2 = 3;
        else if (d >= -16) d2 = 2;
        else d2 = 1;

        if (i >= 8) i2 = 7;
        else if (i >= 5) i2 = 6;
        else if (i >= 2) i2 = 5;
        else if (i >= -1) i2 = 4;
        else if (i >= -4) i2 = 3;
        else if (i >= -8) i2 = 2;
        else i2 = 1;

        if (s >= 12) s2 = 7;
        else if (s >= 8) s2 = 6;
        else if (s >= 5) s2 = 5;
        else if (s >= 1) s2 = 4;
        else if (s >= -2) s2 = 3;
        else if (s >= -7) s2 = 2;
        else s2 = 1;

        if (c >= 6) c2 = 7;
        else if (c >= 3) c2 = 6;
        else if (c >= -1) c2 = 5;
        else if (c >= -3) c2 = 4;
        else if (c >= -7) c2 = 3;
        else if (c >= -11) c2 = 2;
        else c2 = 1;
        return { d2, i2, s2, c2 };

    }

    // Function to create progress charts
    function createCharts() {
        var total = 7;
        var dPercentage = (d2 / total * 100).toFixed(2) + "%";
        var iPercentage = (i2 / total * 100).toFixed(2) + "%";
        var sPercentage = (s2 / total * 100).toFixed(2) + "%";
        var cPercentage = (c2 / total * 100).toFixed(2) + "%";

        document.querySelector("#d-chart").style.width = dPercentage;
        document.querySelector("#i-chart").style.width = iPercentage;
        document.querySelector("#s-chart").style.width = sPercentage;
        document.querySelector("#c-chart").style.width = cPercentage;

        document.querySelector("#d-percentage").textContent = dPercentage;
        document.querySelector("#i-percentage").textContent = iPercentage;
        document.querySelector("#s-percentage").textContent = sPercentage;
        document.querySelector("#c-percentage").textContent = cPercentage;

        return { dPercentage, iPercentage, sPercentage, cPercentage };

    }

    // Function to display results
    function showResults() {
        document.querySelector("#d-score").innerHTML = "(" + d2 + ")";
        document.querySelector("#i-score").innerHTML = "(" + i2 + ")";
        document.querySelector("#s-score").innerHTML = "(" + s2 + ")";
        document.querySelector("#c-score").innerHTML = "(" + c2 + ")";

        return { d2, i2, s2, c2 };

    }

    // Event listener to disable button by default and enable when all radio inputs are selected
    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById('submit').disabled = true;

        function checkAllInputsSelected() {
            var allQuestions = document.querySelectorAll('.q');
            var allSelected = true;

            allQuestions.forEach(function (question) {
                var mostInput = question.querySelector('input[type="radio"][name$="most"]:checked');
                var leastInput = question.querySelector('input[type="radio"][name$="least"]:checked');

                if (!mostInput || !leastInput) {
                    allSelected = false;
                    return;
                }
            });

            return allSelected;
        }

        var radioInputs = document.querySelectorAll('input[type="radio"]');
        for (var i = 0; i < radioInputs.length; i++) {
            radioInputs[i].addEventListener("change", function () {
                if (checkAllInputsSelected()) {
                    document.getElementById('submit').disabled = false;
                } else {
                    document.getElementById('submit').disabled = true;
                }
            });
        }
    });

    // Function to process the form and return the results
    function processForm() {
        var scores = getScores();
        var weightedScores = calculateWeightedScores();
        var charts = createCharts();
        var results = showResults();

        return {
            scores: scores,
            weightedScores: weightedScores,
            charts: charts,
            results: results
        };
    }

    return {
        processForm: processForm
    };

})();
document.addEventListener("DOMContentLoaded", function () {
    var currentQuestionIndex = 0;
    var questions = document.querySelectorAll('.q');
    var backBtn = document.getElementById('backBtn');
    var nextBtn = document.getElementById('nextBtn');
    var submitBtn = document.getElementById('submit');
    var progressBar = document.getElementById('progress-bar');

    function updateNavigationButtons() {
        backBtn.disabled = currentQuestionIndex === 0;
        nextBtn.disabled = currentQuestionIndex === questions.length - 1 || !checkSelectedInputs();
        submitBtn.disabled = !(currentQuestionIndex === questions.length - 1 && checkSelectedInputs());
    }

    function showQuestion(index) {
        for (var i = 0; i < questions.length; i++) {
            questions[i].classList.add('hidden');
        }
        questions[index].classList.remove('hidden');
        updateNavigationButtons();
    }

    function checkSelectedInputs() {
        var currentQuestion = questions[currentQuestionIndex];
        var mostInput = currentQuestion.querySelector('input[type="radio"][name$="most"]:checked');
        var leastInput = currentQuestion.querySelector('input[type="radio"][name$="least"]:checked');

        return mostInput && leastInput;
    }

    nextBtn.addEventListener("click", function () {
        if (currentQuestionIndex < questions.length - 1) {
            currentQuestionIndex++;
            showQuestion(currentQuestionIndex);
        }
    });

    backBtn.addEventListener("click", function () {
        if (currentQuestionIndex > 0) {
            currentQuestionIndex--;
            showQuestion(currentQuestionIndex);
        }
    });

    var radioInputs = document.querySelectorAll('input[type="radio"]');
    for (var i = 0; i < radioInputs.length; i++) {
        radioInputs[i].addEventListener("change", function () {
            updateNavigationButtons();
        });
    }

    submitBtn.addEventListener("click", function () {
        var finalResult = DiscModule.processForm();
        if (currentQuestionIndex === questions.length - 1 && checkSelectedInputs()) {
            // Gather the results
            var formattedResult = {
                finalResult: {
                    "Dominance (D)": finalResult.weightedScores.d2,
                    "Influence (I)": finalResult.weightedScores.i2,
                    "Steadiness (S)": finalResult.weightedScores.s2,
                    "Conscientiousness (C)": finalResult.weightedScores.c2
                }
            };
    

            // Send the results to the server
            fetch('/personality-test', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formattedResult)
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                if (data.finalResult) {
                    console.log('Results saved successfully!');
                } else {
                    console.log('Error saving results.');
                }
            })
            .catch(error => console.error('Error:', error));
        }
    });

    updateNavigationButtons();
});
// Function to handle form submission and make AJAX request
document.querySelector("#submit").onclick = function () {
    var finalResult = DiscModule.processForm();
    makeAjaxRequest(finalResult);
};

// Function to make AJAX request
function makeAjaxRequest(finalResult) {
    var finalScores = calculateFinalScores(finalResult);
    console.log(finalScores); // Output the final scores to verify
    $.ajax({
        url: '/personality-test',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
            'finalResult': formatFinalResult(finalResult.results.innerText)
        }),
        success: function(data) {
            console.log(data);
            var progressBar = document.getElementById('progress-bar');
            var progressPercentageSpan = document.getElementById('progress-percentage');
            progressBar.style.width = '100%';
            progressPercentageSpan.textContent = '100%';
        },
        error: function(error) {
            console.error('Error:', error);
        }
    });
}

// Function to format final result
function formatFinalResult(resultText) {
    var formattedText = resultText.replaceAll(/\s+/g, "");
    var formattedResult = formattedText.replaceAll(/(\w)\((\d+)\)/g, "$1 ($2),");
    return formattedResult.replace(/D/g, "Dominance:D")
                        .replace(/I/g, "Influence:I")
                        .replace(/S/g, "Steadiness:S")
                        .replace(/C/g, "Conscientiousness:C");
}
