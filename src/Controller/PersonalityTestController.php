<?php

namespace Drupal\personality_test\Controller;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\personality_test\Options;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for Personality-Test.
 */
class PersonalityTestController extends ControllerBase {

  /**
   * The block manager service.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Constructs a PersonalityTestController object.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager service.
   */
  public function __construct(BlockManagerInterface $block_manager) {
    $this->blockManager = $block_manager;
  }

  /**
   * Creates a new instance of the PersonalityTestController class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   *   A new instance of the PersonalityTestController class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
        );
  }

  /**
   * Returns content for the Personality Test.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response JSON or renderable array.
   */
  public function content(Request $request) {
    if ($request->getMethod() === 'POST') {
      $content = $request->getContent();
      $data = json_decode($content, TRUE);
      if (isset($data['finalResult'])) {
        $finalResult = $data['finalResult'];
        $tableMarkup = '<table style="border-collapse: collapse; border: 1px solid black;">';
        $tableMarkup .= '<tr style="background-color: rgb(242, 242, 242);"><th style="text-align: center;">Personality Traits</th><th>Score</th></tr>';
        foreach ($finalResult as $trait => $score) {
          $tableMarkup .= '<tr><td style="border: 1px solid black; padding: 5px; width: 100%; text-align: center">' . htmlspecialchars($trait) . '</td>';
          $tableMarkup .= '<td style="border: 1px solid black; padding: 5px;text-align: center;">' . $score . '</td></tr>';
        }
        $tableMarkup .= '</table>';

        // Save to node.
        $user_id = \Drupal::currentUser()->id();
        $node = Node::create([
          'type' => 'personality_assessment_test',
          'title' => 'Personality Assessment Result',
          'field_personality_test_user_id' => $user_id,
          'field_personality_test_result' => [
            'value' => $tableMarkup,
            'format' => 'full_html',
          ],
        ]);
        $node->save();

        // Return JSON response.
        return new JsonResponse(['finalResult' => $tableMarkup]);
      }
      else {
        return new JsonResponse(['error' => 'error'], 400);
      }
    }
    elseif ($request->getMethod() === 'GET') {
      $personalityTest = new Options();
      $data = $personalityTest->getSampleData();
      $build = [
        '#theme' => 'personality_test_quiz',
        '#questions' => $data['questions'],
      ];
      return $build;
    }
    else {
      return new JsonResponse(['error' => 'Invalid request method'], 400);
    }
  }

}
