<?php

namespace Drupal\personality_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'PersonalityTestQuizBlock' block.
 *
 * @Block(
 *   id = "personality_test_quiz_block",
 *   admin_label = @Translation("PersonalityTestQuizBlock"),
 * )
 */
/**
 * Block for Personality-Test.
 *
 * @category CategoryName
 * @package YourPackageName
 * @author Your Name <your_email@example.com>
 * @license https://www.example.com/license YourLicenseName
 * @link https://www.example.com
 * @since PHP 7.4.0
 */
class PersonalityTestQuizBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'personality_test_quiz',
    ];
  }

}
