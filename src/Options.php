<?php

namespace Drupal\personality_test;

/**
 * Provides custom data for the module.
 */
class Options {

  /**
   * Get sample data.
   */
  public function getSampleData() {
    $questions = [
      [
        'number' => 1,
        'traits' => [
          'Enthusiastic',
          'Daring',
          'Diplomatic',
          'Satisfied',
        ],
      ],
      [
        'number' => 2,
        'traits' => [
          'Cautious',
          'Determined',
          'Convincing',
          'Good-Natured',
        ],
      ],
      [
        'number' => 3,
        'traits' => [
          'Friendly',
          'Accurate',
          'Outspoken',
          'Calm',
        ],
      ],
      [
        'number' => 4,
        'traits' => [
          'Talkative',
          'Controlled',
          'Conventional',
          'Decisive',
        ],
      ],
      [
        'number' => 5,
        'traits' => [
          'Adventurous',
          'Insightful',
          'Outgoing',
          'Moderate',
        ],
      ],
      [
        'number' => 6,
        'traits' => [
          'Gentle',
          'Persuasive',
          'Humble',
          'Original',
        ],
      ],
      [
        'number' => 7,
        'traits' => [
          'Expressive',
          'Conscientious',
          'Dominant',
          'Responsive',
        ],
      ],
      [
        'number' => 8,
        'traits' => [
          'Poised',
          'Observant',
          'Modest',
          'Impatient',
        ],
      ],
      [
        'number' => 9,
        'traits' => [
          'Tactful',
          'Agreeable',
          'Magnetic',
          'Insistent',
        ],
      ],
      [
        'number' => 10,
        'traits' => [
          'Brave',
          'Inspiring',
          'Submissive',
          'Timid',
        ],
      ],
      [
        'number' => 11,
        'traits' => [
          'Reserved',
          'Obliging',
          'Strong-Willed',
          'Cheerful',
        ],
      ],
      [
        'number' => 12,
        'traits' => [
          'Stimulating',
          'Kind',
          'Perceptive',
          'Independent',
        ],
      ],
      [
        'number' => 13,
        'traits' => [
          'Competitive',
          'Considerate',
          'Joyful',
          'Private',
        ],
      ],
      [
        'number' => 14,
        'traits' => [
          'Fussy',
          'Obedient',
          'Firm',
          'Playful',
        ],
      ],
      [
        'number' => 15,
        'traits' => [
          'Attractive',
          'Introspective',
          'Stubborn',
          'Predictable',
        ],
      ],
      [
        'number' => 16,
        'traits' => [
          'Logical',
          'Bold',
          'Loyal',
          'Charming',
        ],
      ],
      [
        'number' => 17,
        'traits' => [
          'Sociable',
          'Patient',
          'Self-Reliant',
          'Soft Spoken',
        ],
      ],
      [
        'number' => 18,
        'traits' => [
          'Willing',
          'Eager',
          'Thorough',
          'High-Spirited',
        ],
      ],
      [
        'number' => 19,
        'traits' => [
          'Aggressive',
          'Extraverted',
          'Amiable',
          'Fearful',
        ],
      ],
      [
        'number' => 20,
        'traits' => [
          'Confident',
          'Sympathetic',
          'Impartial',
          'Assertive',
        ],
      ],
      [
        'number' => 21,
        'traits' => [
          'Well-Disciplined',
          'Generous',
          'Animated',
          'Persistent',
        ],
      ],
      [
        'number' => 22,
        'traits' => [
          'Impulsive',
          'Introverted',
          'Forceful',
          'Easy-Going',
        ],
      ],
      [
        'number' => 23,
        'traits' => [
          'Good Mixer',
          'Refined',
          'Vigorous',
          'Lenient',
        ],
      ],
      [
        'number' => 24,
        'traits' => [
          'Captivating',
          'Contented',
          'Demanding',
          'Compliant',
        ],
      ],
      [
        'number' => 25,
        'traits' => [
          'Argumentative',
          'Systematic',
          'Cooperative',
          'Light-Hearted',
        ],
      ],
      [
        'number' => 26,
        'traits' => [
          'Jovial',
          'Precise',
          'Direct',
          'Even-Tempered',
        ],
      ],
      [
        'number' => 27,
        'traits' => [
          'Restless',
          'Neighborly',
          'Appealing',
          'Careful',
        ],
      ],
      [
        'number' => 28,
        'traits' => [
          'Respectful',
          'Pioneering',
          'Optimistic',
          'Helpful',
        ],
      ],
    ];
    return [
      'questions' => $questions,
    ];
  }

}
